// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

let todo = fetch("https://jsonplaceholder.typicode.com/todos");
todo
  .then((response) => {
    return response.json();
  })
  .then((result) => {
    let arr = result.map((elements) => {
      return elements.userId;
    });
    arr.forEach((element) => {
      const ans = fetch(
        `https://jsonplaceholder.typicode.com/users?id=${element}`
      );
      ans
        .then((result) => {
          return result.json();
        })
        .then((ans) => {
          console.log(ans);
        });
    });
  });
