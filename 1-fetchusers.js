// Using promises and the `fetch` library, do the following.
// 1. Fetch all the users

let promise1 = fetch("https://jsonplaceholder.typicode.com/users");

promise1
  .then((response) => {
    return response.json();
  })
  .then(result => {
    console.log(result);
  })
  .catch(err=>{
    console.error(err);
  })