// 3. Use the promise chain and fetch the users first and then the todos.

let promise1 = fetch("https://jsonplaceholder.typicode.com/users");

promise1
  .then((response) => {
    return response.json();
  })
  .then((result) => {
    console.log("User data :-");
    console.log(result);

    return fetch("https://jsonplaceholder.typicode.com/todos");
  })
  .then((response) => {
    return response.json();
  })
  .then((result) => {
    console.log("Todo data :-");
    console.log(result);
  })
  .catch((err) => {
    console.error(err);
  });
