// 4. Use the promise chain and fetch the users first and then all the details for each user.

let users = fetch("https://jsonplaceholder.typicode.com/users");

users
  .then((response) => {
    return response.json();
  })
  .then((result) => {
    let arr = result.map((elements) => {
      return elements.id;
    });
    arr.forEach((element) => {
      const ans = fetch(
        `https://jsonplaceholder.typicode.com/users?id=${element}`
      );
      ans
        .then((result) => {
          return result.json();
        })
        .then((ans) => {
          console.log(ans);
        });
    });
  });
