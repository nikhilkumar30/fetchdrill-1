// 2. Fetch all the todos

let promise2 =fetch("https://jsonplaceholder.typicode.com/todos");

promise2.then((response)=>{
    return response.json();
}).then((result)=>{
    console.log(result);
})
.catch(err=>{
    console.error(err);
})